#!/bin/bash
# Usage: map_loosely_on_refs.sh <library_name> <sub-directory> <reads.fastq> <reads_category_name> <refs> <refs_name> [<no_process>]
# Set <no_process> to anything to skip sam file processing and compression

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

function make_bam_index
{
    if [ ! -e ${1%.sam}_sorted.bam -o ! -e ${1%.sam}_sorted.bam.bai ]
    then
        nice -n 19 ionice -c2 -n7 sam2indexedbam.sh ${1} || error_exit "sam2indexedbam.sh failed"  
    fi
}


# The library name, and directory in which results will be written
name=${1}
# The name of the sub-directory in which the results will be written
sub_dir=${2}
# The fastq(.gz) file of the reads that have to be mapped
fastq_in=${3}
# A name to designate the type of reads, used to make output names
name_in=${4}
# The name of the bowtie2 index file to use for the mapping (full path except extensions)
ref=${5}
# A name to designate the type of references on which the reads have to be mapped, also used to make output names
name_on=${6}

name_out="${name_in}_on_${name_on}"

if [ ${sub_dir} == "${name}" ]
then
    # We will not create a sub-directory if the name is the same as the directory
    # Will contain .sam mapping results
    map_dir="${name}/mapped_${name_out}"
    # Will contain the sequences that mapped, in distinct files for distinct references
    seq_dir="${name}/reads_${name_out}"
    # Will contain size histogram data for the various references
    histo_dir="${name}/histos_${name_out}"
else
    # Create the sub-directory (if not already existing)
    mkdir -p ${sub_dir}
    # Will contain .sam mapping results
    map_dir="${sub_dir}/mapped_${name_out}"
    # Will contain the sequences that mapped, in distinct files for distinct references
    seq_dir="${sub_dir}/reads_${name_out}"
    # Will contain size histogram data for the various references
    histo_dir="${sub_dir}/histos_${name_out}"
fi

mkdir -p ${map_dir}
map_file="${map_dir}/${name}_${name_out}.sam"

if [ -e ${map_file}.bz2 ]
then
    if [ ! -e ${map_file} ]
    then
        echo -e "\nUncompressing existing ${map_file}.bz2"
        echo "Be sure there is enough disk space for that."
        cmd="lbunzip2 -k ${map_file}.bz2"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    fi
fi
if [ ! -e ${map_file} ]
then
    echo "Mapping ${name_in} on ${name_on}."
    cmd="bowtie2 --seed 123 -t -k 10 -N 1 -x ${ref} -U ${fastq_in} -S ${map_file}"
    echo ${cmd}
    eval ${cmd} || error_exit "bowtie2 failed"
else
    echo "Re-using already existing file ${map_file}."
fi

# Make a sorted and indexed bam version in the background if necessary
# Log this separately to re-use later
tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
make_bam_index ${map_file} > ${tmpdir}/indexing.log 2> ${tmpdir}/indexing.err &


if [ $# = 7 ]
then
    # <no_process> option is set
    exit 0
else
    # Extracting reads and building size histograms
    #mkdir ${seq_dir} || error_exit "mkdir failed"
    #mkdir ${histo_dir} || error_exit "mkdir failed"
    if [ -e ${seq_dir} ]
    then
        echo "Warning: Content of ${seq_dir} will be overwritten"
        rm -rf ${seq_dir}
    fi
        mkdir ${seq_dir}
    if [ -e ${seq_dir} ]
    then
        echo "Warning: Content of ${histo_dir} will be overwritten"
        rm -rf ${histo_dir}
    fi
    mkdir ${histo_dir}

    ###
    # For each mapped read, take the best ex-aequo alignments that are above the minimum score.
    # They may be on different references.
    # For each of these references:
    # - Classify as perfect match if at least one of the alignments has ali.opt("NM") == 0 (no mismatches)
    # - Classify as mismatch else
    # - Write the read in the .fastq corresponding to the ref x mismatch category (No Forward vs. Reverse information yet)
    # - Count among the equally least mismatched the proportion of F and R mappers on the ref, and add this to the histograms
    ###

    echo "Processing mapping results."
    cmd="read_sam.py --min_score '-10' --in_file ${map_file} --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
    echo ${cmd}
    eval ${cmd} || error_exit "read_sam.py failed"
    # This will generate fastq files:
    # ${seq_dir}/${name}_${name_in}_on_<ref>.fastq
    # ${seq_dir}/${name}_${name_in}_on_<ref>_low_score.fastq
    # Size histograms:
    # ${hist_dir}/${name}_${name_in}_on_<ref>_lengths.txt
    # Reference countings:
    # ${hist_dir}/${name}_${name_in}_on_${name_on}_ref_counts.txt
    echo -e "\nRead counts for every reference present in ${name_on} are recorded in the following table:"
    echo "${histo_dir}/${name}_${name_in}_on_${name_on}_ref_counts.txt"
    echo -e "\nFor each reference <ref>, some files have been generated:"
    echo "Mapping sequences: ${seq_dir}/${name}_${name_in}_on_<ref>.fastq"
    echo "Read length histograms: ${histo_dir}/${name}_${name_in}_on_<ref>_lengths.txt"

    # Saving space

    # Note that in some cases (small files), compressing will increase space usage.
    echo -e "\nCompressing generated fastq files with pigz"
    #for file in ${seq_dir}/*.fastq
    #do
    #    pigz ${file} || error_exit "pigz failed"
    #done
    # Maybe quicker (only one instance of pigz):
    #pigz ${seq_dir}/*.fastq || error_exit "pigz failed"
    # To not apply compression to files of size less than 511
    find ${seq_dir} -type f -name *fastq -size +511c -print0 | xargs -0 pigz || error_exit "pigz failed"

    # Wait for the end of map file indexing before saving space
    wait
    # cat the indexing logs to stdout and stderr
    cat ${tmpdir}/indexing.log
    (1>&2 cat ${tmpdir}/indexing.err)
    rm -rf ${tmpdir}
    if [ ! -e ${map_file}.bz2 ]
    then
        echo -e "\nCompressing ${map_file} with lbzip2"
        cmd="lbzip2 ${map_file}"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    else
            echo -e "\nRemoving ${map_file} file, of which a compressed ${map_file}.bz2 version exists"
            rm -f ${map_file}
    fi
fi

exit 0
