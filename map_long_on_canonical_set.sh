#!/bin/bash
# Usage: map_on_canonical_set.sh <library_name> <G_species>

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# library name
name=${1}
echo $name

genome=${2}
echo ${genome}

# file containing the counts used to perform normalizations
#norm_file=${2}
#norm=`cat ${norm_file}`

#ncpu=${3}

seq_dir="${name}/mapped_reads_${genome}"

TE=${seq_dir}/${name}_on_${genome}_TE.fastq

refs="${genomes_path}/${genome}/Annotations/TE_plus"

echo -e "\nMapping TE long reads on canonical TEs."
./map_long_loosely_on_refs.sh ${name} ${name} ${TE} "TE" ${refs} "TE"


## extracting reads and building size histograms
#
## Will contain the sequences that mapped, in distinct files for distinct annotation groups
#mkdir -p mapped_reads_TE/${name}
#seq_dir=mapped_reads_TE/${name}
## Will contain size histogram data for the various annotation groups
#mkdir -p histos_mapped_TE/${name}
#histo_dir=histos_mapped_TE/${name}
#
####
## For each mapped read, take the best ex-aequo alignments that are above the minimum score.
## They may be on different references.
## For each of these references:
## - Classify as perfect match if at least one of the alignments has ali.opt("NM") == 0 (no mismatches)
## - Classify as mismatch else
## - Write the read in the .fastq corresponding to the ref x mismatch category (No Forward vs. Reverse information yet)
## - Count among the equally least mismatched the proportion of F and R mappers on the ref, and add this to the histograms
####
#
#echo "read_sam.py --min_score "-10" --in_file mapped_TE/${name}_piRNA_candidate_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_TE/${name}_piRNA_candidate_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
## This will generate fastq files:
## ${seqdir}/${name}_piRNA_candidate_on_<TE>_0mm.fastq
## ${seqdir}/${name}_piRNA_candidate_on_<TE>_mm.fastq
## ${seqdir}/${name}_piRNA_candidate_on_<TE>_low_score.fastq
#
#echo "read_sam.py --min_score "-10" --in_file mapped_TE/${name}_siRNA_candidate_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_TE/${name}_siRNA_candidate_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
## This will generate fastq files:
## ${seqdir}/${name}_siRNA_candidate_on_<TE>_0mm.fastq
## ${seqdir}/${name}_siRNA_candidate_on_<TE>_mm.fastq
## ${seqdir}/${name}_siRNA_candidate_on_<TE>_low_score.fastq
#
#echo "read_sam.py --min_score "-10" --in_file mapped_TE/${name}_pi-si-TE-3UTR_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir}"
#read_sam.py --min_score "-10" --in_file mapped_TE/${name}_pi-si-TE-3UTR_on_TE.sam --seq_dir ${seq_dir} --hist_dir ${histo_dir} || error_exit "read_sam.py failed"
## This will generate fastq files:
## ${seqdir}/${name}_pi-si-TE-3UTR_on_<TE>_0mm.fastq
## ${seqdir}/${name}_pi-si-TE-3UTR_on_<TE>_mm.fastq
## ${seqdir}/${name}_pi-si-TE-3UTR_on_<TE>_low_score.fastq
#
#
## TODO: this part should go in another script, because it uses results from different libraries (in order to scale the histograms)
## making histograms for some transposable elements (Would it be possible to add some 3'UTRS and pi_clusters in the loop too?)
##for TE in "1731" "17.6" "batumi" "bel" "BS2" "297" "doc" "F-element" "G6" "HetA" "I-element" "invader4" "max" "nomad" "protoP_A" "R1" "RT1A" "RT1B" "TAHRE" "TART"
##do
##    plot_histos.py --in_files histos_mapped_TE/drosha_dmt/drosha_dmt_on_${TE}_lengths.txt \
##        histos_mapped_TE/drosha_dw/drosha_dw_on_${TE}_lengths.txt \
##        histos_mapped_TE/drosha_wt/drosha_wt_on_${TE}_lengths.txt \
##        histos_mapped_TE/Gtsf1_hom/Gtsf1_hom_on_${TE}_lengths.txt \
##        histos_mapped_TE/Gtsf1_het/Gtsf1_het_on_${TE}_lengths.txt \
##        --normalizations drosha_dmt_strict_on_D_melanogaster_piRNA_cluster1.nbseq \
##        drosha_dw_strict_on_D_melanogaster_piRNA_cluster1.nbseq \
##        drosha_wt_strict_on_D_melanogaster_piRNA_cluster1.nbseq \
##        Gtsf1_hom_strict_on_D_melanogaster_piRNA_cluster1.nbseq \
##        Gtsf1_het_strict_on_D_melanogaster_piRNA_cluster1.nbseq \
##        --hist_dir histos_pdf --y_axis "reads per million 42AB strict mappers"
##done
#
#
exit 0
