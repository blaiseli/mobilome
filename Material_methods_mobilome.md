"Mobilome" reads were mapped with Bowtie2 version 2.2.4 (using parameters -k 10
-N 1) on the Drosophila melanogaster genome (dm3) complemented with canonical
transposable elements (TEs).

The total number of reads having their best-score alignment on the
mitochondrial genome was determined in order to be used for read counts
normalization. Mitochondrial reads were chosen for normalization because they
come from circular DNA, and are thus expected to be present in a "mobilome"
library.

Reads having their best-score alignment either in genomic copies of TEs or
canonical TEs were annotated as TE reads.

Such reads were then mapped again using the canonical TEs only as references,
with Bowtie2 and the same parameters as above. Read alignments with a mapping
score strictly lower than -10 were discarded. The mapping location of
best-score alignments is was used to attribute each read to a TE family. If a
read had several ex-aequo best-score alignments, the read count associated to
each corresponding mapping location was down-weighted accordingly. 

A scatterplot was used to compare the canonical TE having a resulting total
read count at least 10 in both libraries. A normalization by thousands of mtDNA
reads was applied to account for library depth differences between the
"no-KD" and "piwi-sKD" libraries. Black dots represent LTR TEs and red dots
represent NLR and DNA TEs.

Mapping results processing and scatterplot generation were performed using the
pysam and matplotlib Python libraries. 

See [README_mobilome.md](README_mobilome.md) for more details about the
commands that were used.
