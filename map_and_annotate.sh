#!/bin/bash
# Usage: map_and_annotate.sh <library_name> <G_species> [<annotation_file>]

# To configure according to the local installation:
genomes_path=${GENOMES}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

function make_bam_index
{
    if [ ! -e ${1%.sam}_sorted.bam -o ! -e ${1%.sam}_sorted.bam.bai ]
    then
        nice -n 19 ionice -c2 -n7 sam2indexedbam.sh ${1} || error_exit "sam2indexedbam.sh failed"  
    fi
    if [ ! -e ${1%.sam}.bed ]
    then
         nice -n 19 ionice -c2 -n7 bedtools bamtobed -ed -i ${1%.sam}_sorted.bam > ${1%.sam}.bed || error_exit "bedtools bamtobed failed"
    fi
}


# library name
name=${1}
echo "Library: ${name}"

genome=${2}
echo "Genome: ${genome}"
#genome_db=${genomes_path}/${genome}/${genome}
genome_db="${genomes_path}/${genome}/${genome}_and_TE"
#annotation_file="${genomes_path}/${genome}/Annotations/${genome}_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_annot.txt"
#annotation_file="${genomes_path}/D_melanogaster/Annotations/Flybase/D_melanogaster_flybase_splitmRNA_annot.txt"
if [ $# = 3 ]
then
    annotation_file=${3}
else
    # This .bed file must have been compressed with bgzip and indexed with tabix
    annotation_file="${genome}_flybase_mt_and_TE_annot.bed"
fi

echo "Annotation file: ${annotation_file}"

# TODO: this is not used any more with the tabix annotation system -> re-implement ?
#exclude_types="mRNA,ncRNA,CDS,five_prime_untranslated_region"
exclude_types="None"
echo "Types excluded from annotation: ${exclude_types}"

# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
annot_module="${DIR}/annotation_mobilome.py"
echo "Module used to define annotation priorities: ${annot_module}"

#ncpu=${3}

fastq_in="raw_data/${name}/${name}.fastq"
if [ ! -e ${fastq_in} ]
then
    echo -e "\nThis script requires that the sequences are present in ${fastq_in}"
    error_exit "Could not find raw data."
fi

#########################
# mapping on the genome #
#########################

# Will contain the .sam mapping result
map_dir="${name}/mapped_${genome}"
mkdir -p ${map_dir}
# Write them in the mapped_${genome} result while processing the .sam file
## Will contain the sequences that did not map
#mkdir -p unmapped_${genome}/${name}

sam_file="${map_dir}/${name}_on_${genome}.sam"
if [ ! -e ${sam_file} ]
then 
    if [ -e ${sam_file}.bz2 ]
    then
        echo -e "\nUncompressing existing ${sam_file}.bz2"
        echo "Be sure there is enough disk space for that."
        cmd="lbunzip2 -k ${sam_file}.bz2"
        echo ${cmd}
        eval ${cmd} || error_exit "${cmd} failed"
    else
        echo -e "\nMapping ${fastq_in} on ${genome_db} with bowtie2"
        #cmd="bowtie2 --seed 123 -t -k 10 -L 6 -i S,1,0.8 -N 1 --mp 4,2 --rdg 5,2 --rfg 5,2"
        # For long reads, it is probably better not to tweak to much the parameters as for short reads.
        cmd="bowtie2 --seed 123 -t -k 10 --no-unal -N 1"
        echo "${cmd} -x ${genome_db} -U ${fastq_in} -S ${sam_file}"
        eval ${cmd} -x ${genome_db} -U ${fastq_in} -S ${sam_file} || error_exit "${cmd} failed"
    fi
else
    echo -e "\nUsing already existing ${sam_file}"
fi

# Make a sorted and indexed bam version in the background if necessary
# Log this separately to re-use later
tmpdir=$(mktemp -dt "`basename $0`.XXXXXXXXXX")
make_bam_index ${sam_file} > ${tmpdir}/indexing.log 2> ${tmpdir}/indexing.err &

#############################################################################
# Annotating the reads and generating fastq files for each annotation group #
#############################################################################

# Will contain the sequences that mapped, in distinct files for distinct annotation groups
seq_dir="${name}/mapped_reads_${genome}"
mkdir -p ${seq_dir}
# Will contain size histogram data for the various annotation groups
histo_dir="${name}/histos_mapped_${genome}"
mkdir -p ${histo_dir}

echo -e "\nProcessing results from ${sam_file}"
#--min_score -10 was chosen in order to filter out alignments with too many mismatches.
# We had to use this proxy because filtering on the number of mismatches
# sometimes excluded the primary alignment.
# Also, the score is in theory a more suitable criterion than the number of mismatches,
# because it takes into account read quality.
#TODO: Better deal with exclude_types (take from file ?)
cmd="annotate_sam.py --in_file ${sam_file} --annot ${annotation_file} --annot_module ${annot_module} --exclude_types ${exclude_types} --min_score "-10" --pi_range 23,30 --seq_dir ${seq_dir} --histo_dir ${histo_dir}"
echo ${cmd}
time eval ${cmd} || error_exit "${cmd} failed"


# Wait for the end of map file indexing before saving space
echo -e "\nWaiting for ${sam_file} indexation to complete."
wait
# cat the indexing logs to stdout and stderr
cat ${tmpdir}/indexing.log
(1>&2 cat ${tmpdir}/indexing.err)
rm -rf ${tmpdir}

# Saving space
#if [ ! -e ${sam_file}.bz2 ]
#then
#    echo -e "\nCompressing ${sam_file} with lbzip2"
#    cmd="lbzip2 ${sam_file}"
#    echo ${cmd}
#    eval ${cmd} || error_exit "${cmd} failed"
#else
#        echo -e "\nRemoving ${sam_file} file, of which a compressed ${sam_file}.bz2 version exists"
#        rm -f ${sam_file}
#fi

exit 0
