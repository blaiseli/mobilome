What you will find here (Foreword added 22/02/2018)
===================================================

What follows is mostly a lightly commented log of the commands that were used
to analyse the mobilome data for Severine Chambeyron. It is provided as is, in
the hope that it can be helpful for those who would like to do similar analyses
and have examples of how the scripts are used.

The library named "no-KD" in the paper corresponds to "C2Gn_embryons" here, and
the one named "piwi_sKD" corresponds to "C2GnS1_embryons".

Some of the scripts (that are not called by their local `./` path:
`build_annotated_genome.py`, `do_scatterplot.sh` and their possible
dependencies) actually come from another pipeline, and can be obtained here:
<https://bitbucket.org/blaiseli/pirna-pipeline>. The description of that
pipeline also covers the installation of some of the genomic data that are used
here. Other tools are publicly available UNIX utilities or bioinformatics
programs (`l` is an alias for `ls -lhrt`).

Blaise Li (<blaise.li@normalesup.org>)


Initial data exploration phase
==============================

Raw data:

    l ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/
    total 4.3G
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 592M Feb 27  2014 S1_S10_L001_R2_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 592M Feb 27  2014 S1_S10_L001_R1_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 409M Feb 27  2014 S2_S11_L001_R2_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 410M Feb 27  2014 S2_S11_L001_R1_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 582M Feb 27  2014 S3_S12_L001_R2_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 581M Feb 27  2014 S3_S12_L001_R1_001.fastq*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 214M Feb 27  2014 S1_S10_L001_R1_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 213M Feb 27  2014 S1_S10_L001_R2_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 156M Feb 27  2014 S2_S11_L001_R1_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 152M Feb 27  2014 S2_S11_L001_R2_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 218M Feb 27  2014 S3_S12_L001_R1_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron 224M Feb 27  2014 S3_S12_L001_R2_001.fastq.gz*
    -rwxr-xr-x 1 severine.chambeyron severine.chambeyron   65 Feb 27  2014 00README_ADN_circulaires_sequences.txt*

    mkdir raw_data
    mkdir raw_data/S13_20cst
    mkdir raw_data/S13_25shift
    zcat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq.gz ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R2_001.fastq.gz > raw_data/S13_20cst/S13_20cst.fastq

Problems when decompressing.

    gzip: /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq.gz: invalid compressed data--crc error

    gzip: /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq.gz: invalid compressed data--length error


Checking the uncompressed data:


    severine.chambeyron@yoshi:~/projet_alain_shpiwi/mobilome$ md5sum /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/*.fastq
    73229e72e1a875c98c0e306774255c23  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq
    43b5b926d1eddbd4dcbac5f25d5d53f3  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R2_001.fastq
    68c91ddeab48c5038181f498c470cbfc  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R1_001.fastq
    96547484caae5af41d1e60cdaf21798b  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R2_001.fastq
    2e1267b70fa097668999ef2c0707c338  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S3_S12_L001_R1_001.fastq
    9a5098bb6765e2454da6814358cee0a9  /home/severine.chambeyron/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S3_S12_L001_R2_001.fastq

    bli@sei-lot:~/Documents/Equipe_Chambeyron/cercles$ md5sum /home/bli/raw_data/Equipe_Chambeyron/cercles/*.fastq
    73229e72e1a875c98c0e306774255c23  /home/bli/raw_data/Equipe_Chambeyron/cercles/S1_S10_L001_R1_001.fastq
    43b5b926d1eddbd4dcbac5f25d5d53f3  /home/bli/raw_data/Equipe_Chambeyron/cercles/S1_S10_L001_R2_001.fastq
    68c91ddeab48c5038181f498c470cbfc  /home/bli/raw_data/Equipe_Chambeyron/cercles/S2_S11_L001_R1_001.fastq
    96547484caae5af41d1e60cdaf21798b  /home/bli/raw_data/Equipe_Chambeyron/cercles/S2_S11_L001_R2_001.fastq
    2e1267b70fa097668999ef2c0707c338  /home/bli/raw_data/Equipe_Chambeyron/cercles/S3_S12_L001_R1_001.fastq
    9a5098bb6765e2454da6814358cee0a9  /home/bli/raw_data/Equipe_Chambeyron/cercles/S3_S12_L001_R2_001.fastq
    f79f194673b981858e28deb7ac12ae93  /home/bli/raw_data/Equipe_Chambeyron/cercles/S4_S8_L001_R1_001.fastq
    1e69bb791a2011fa3553f3c0a6988346  /home/bli/raw_data/Equipe_Chambeyron/cercles/S4_S8_L001_R2_001.fastq
    2526a7a81ad652a93d103b222c81f40e  /home/bli/raw_data/Equipe_Chambeyron/cercles/S5_S9_L001_R1_001.fastq
    50de4be58a240f6dffe9ff10593284f8  /home/bli/raw_data/Equipe_Chambeyron/cercles/S5_S9_L001_R2_001.fastq

Uncompressed data seems OK.

[//]: # (Note: Flies S13_20cst and S13_25shift should be named "697S13Gn" and "697S13GnS1" respectively)

    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R2_001.fastq > raw_data/S13_20cst/S13_20cst.fastq
    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R1_001.fastq ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R2_001.fastq > raw_data/S13_25shift/S13_25shift.fastq

This makes many read names appear twice in the raw data, which causes problems later in the analyses.

    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R1_001.fastq | sed 's/ /_/' > raw_data/697S13Gn/697S13Gn.fastq
    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S1_S10_L001_R2_001.fastq | sed 's/ /_/' >> raw_data/697S13Gn/697S13Gn.fastq
    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R1_001.fastq | sed 's/ /_/' > raw_data/697S13GnS1/697S13GnS1.fastq
    cat ~/raw_data/Raw_data_Chambeyron/ADN_circulaire_26_2_14/S2_S11_L001_R2_001.fastq | sed 's/ /_/' > raw_data/697S13GnS1/697S13GnS1.fastq

Making annotation files:

    ln -s ~/Genomes/D_melanogaster/Annotations/Flybase/mitoch*.fa .
    ln -s ~/Genomes/D_melanogaster/Annotations/TE_extended.fa .
    bowtie2-build --seed 123 --packed mitochtRNA.fa,mitochmiscRNA.fa,mitochCDS.fa,TE_extended.fa D_melanogaster_flybase_mt_and_TE
    build_annotated_genome.py \
        --annotations mitochCDS.fa,mitochmiscRNA.fa,mitochtRNA.fa \
        --genome D_melanogaster_flybase_mt_and_TE \
        --fasta_TE TE_extended.fa
    bgzip -c D_melanogaster_flybase_mt_and_TE_annot.bed > D_melanogaster_flybase_mt_and_TE_annot.bed.bgz
    tabix -p bed D_melanogaster_flybase_mt_and_TE_annot.bed.bgz

Mapping:

    ./map_and_annotate.sh 697S13Gn D_melanogaster > logs/697S13Gn/697S13Gn.stdout 2> logs/697S13Gn/697S13Gn.stderr
    ./map_and_annotate.sh 697S13GnS1 D_melanogaster > logs/697S13GnS1/697S13GnS1.stdout 2> logs/697S13GnS1/697S13GnS1.stderr
    
TE counting:

    ./map_long_on_canonical_set.sh 697S13Gn D_melanogaster >> logs/697S13Gn/697S13Gn.stdout 2>> logs/697S13Gn/697S13Gn.stderr
    ./map_long_on_canonical_set.sh 697S13GnS1 D_melanogaster >> logs/697S13GnS1/697S13GnS1.stdout 2>> logs/697S13GnS1/697S13GnS1.stderr

Counting mitochondrial reads for normalization:

    cat 697S13Gn/mapped_reads_D_melanogaster/697S13Gn_on_D_melanogaster_rRNA.fasta 697S13Gn/mapped_reads_D_melanogaster/697S13Gn_on_D_melanogaster_tRNA.fasta 697S13Gn/mapped_reads_D_melanogaster/697S13Gn_on_D_melanogaster_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > 697S13Gn/mapped_D_melanogaster/697S13Gn_on_D_melanogaster_mt.nbseq
    cat 697S13GnS1/mapped_reads_D_melanogaster/697S13GnS1_on_D_melanogaster_rRNA.fasta 697S13GnS1/mapped_reads_D_melanogaster/697S13GnS1_on_D_melanogaster_tRNA.fasta 697S13GnS1/mapped_reads_D_melanogaster/697S13GnS1_on_D_melanogaster_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > 697S13GnS1/mapped_D_melanogaster/697S13GnS1_on_D_melanogaster_mt.nbseq

Making scatterplots:

    NO_LINE="Yes" MIN_COUNTS="10" do_scatterplot.sh 697S13Gn 697S13GnS1 TE TE mt log2 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt blue,green,red 2
    NO_LINE="Yes" MIN_COUNTS="1" do_scatterplot.sh 697S13Gn 697S13GnS1 TE TE mt log2 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt blue,green,red 2


New analyses (22/07/2015, then 01/10/2015 for C2Gn and C2GnS1)
==============================================================

Preparing raw data
------------------

### Fusing the two files for each library, becaus we don't use the fact that the sequencing was paired-end.


    ``` bash
    cd ~/projet_alain_shpiwi/mobilome/raw_data
    bash README_raw_data_mobilome.txt
    l 697S13Gn*_*s/
    ```

Output:

    697S13Gn_embryons/:
    total 1.2G
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 1.2G Jul 22 12:55 697S13Gn_embryons.fastq

    697S13GnS1_embryons/:
    total 818M
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 818M Jul 22 12:55 697S13GnS1_embryons.fastq

    697S13Gn_ovaires/:
    total 671M
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 671M Jul 22 12:55 697S13Gn_ovaires.fastq

    697S13GnS1_ovaires/:
    total 732M
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 732M Jul 22 12:55 697S13GnS1_ovaires.fastq


Addendum 01/10/2015
-------------------

    ```bash
    # Add the commands for prepararing C2Gn[S1]
    vim README_raw_data_mobilome.txt
    # Execute the added lines
    cat README_raw_data_mobilome.txt | tail -12 | bash
    l C2Gn*
    ```

Output:

    C2Gn_embryons:
    total 1.2G
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 1.2G Oct  1 12:17 C2Gn_embryons.fastq

    C2GnS1_embryons:
    total 1.4G
    -rw-r--r-- 1 severine.chambeyron severine.chambeyron 1.4G Oct  1 12:17 C2GnS1_embryons.fastq



Making annotation files
-----------------------

    cd ~/projet_alain_shpiwi/mobilome
    ln -s ~/Genomes/D_melanogaster/Annotations/Flybase/mitoch*.fa .
    ln -s ~/Genomes/D_melanogaster/Annotations/TE_extended.fa .
    bowtie2-build --seed 123 --packed mitochtRNA.fa,mitochmiscRNA.fa,mitochCDS.fa,TE_extended.fa D_melanogaster_flybase_mt_and_TE
    build_annotated_genome.py \
        --annotations mitochCDS.fa,mitochmiscRNA.fa,mitochtRNA.fa \
        --genome D_melanogaster_flybase_mt_and_TE \
        --fasta_TE TE_extended.fa
    bgzip -c D_melanogaster_flybase_mt_and_TE_annot.bed > D_melanogaster_flybase_mt_and_TE_annot.bed.bgz
    tabix -p bed D_melanogaster_flybase_mt_and_TE_annot.bed.bgz


Mapping
-------

!! Not sure the above-constructed bowtie2 index was used (see
`map_and_annotate.sh`). Instead, the mapping has been done on the full genome,
complemented by canonical TE sequences. This includes mitochondrial chromosome.



``` bash
for lib in "697S13Gn_embryons" "697S13GnS1_embryons" "697S13Gn_ovaires" "697S13GnS1_ovaires"
do
    mkdir -p logs/${lib}
    ./map_and_annotate.sh ${lib} D_melanogaster > logs/${lib}/${lib}.stdout 2> logs/${lib}/${lib}.stderr
done
```

Addendum 01/10/2015
-------------------

``` bash
for lib in "C2Gn_embryons" "C2GnS1_embryons"
do
    mkdir -p logs/${lib}
    ./map_and_annotate.sh ${lib} D_melanogaster > logs/${lib}/${lib}.stdout 2> logs/${lib}/${lib}.stderr
done
```


TE counting
-----------

``` bash
for lib in "697S13Gn_embryons" "697S13GnS1_embryons" "697S13Gn_ovaires" "697S13GnS1_ovaires"
do
    ./map_long_on_canonical_set.sh ${lib} D_melanogaster >> logs/${lib}/${lib}.stdout 2>> logs/${lib}/${lib}.stderr
done
```


Addendum 02/10/2015
-------------------

``` bash
for lib in "C2Gn_embryons" "C2GnS1_embryons"
do
    ./map_long_on_canonical_set.sh ${lib} D_melanogaster >> logs/${lib}/${lib}.stdout 2>> logs/${lib}/${lib}.stderr
done
```


Counting mitochondrial reads for normalization
----------------------------------------------

``` bash
for lib in "697S13Gn_embryons" "697S13GnS1_embryons" "697S13Gn_ovaires" "697S13GnS1_ovaires"
do
    cat ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_rRNA.fasta ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_tRNA.fasta ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > ${lib}/mapped_D_melanogaster/${lib}_on_D_melanogaster_mt.nbseq
done
```

Addendum 02/10/2015
-------------------

``` bash
for lib in "C2Gn_embryons" "C2GnS1_embryons"
do
    cat ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_rRNA.fasta ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_tRNA.fasta ${lib}/mapped_reads_D_melanogaster/${lib}_on_D_melanogaster_CDS.fasta | grep "^>" | awk '{print $1}' | sort | uniq | wc -l > ${lib}/mapped_D_melanogaster/${lib}_on_D_melanogaster_mt.nbseq
done
```


Making scatterplots
-------------------

``` bash
for lib_type in "embryons" "ovaires"
do
    BY="1000" NO_LINE="Yes" MIN_COUNTS="10" do_scatterplot.sh "697S13Gn_${lib_type}" "697S13GnS1_${lib_type}" TE TE mt log10 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt blue,green,red 2
done
```

Addendum 02/10/2015
-------------------

``` bash
BY="1000" NO_LINE="Yes" MIN_COUNTS="10" do_scatterplot.sh "C2Gn_embryons" "C2GnS1_embryons" TE TE mt log10 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt blue,green,red 2
```


Figures for _Droso 2015_ congress
---------------------------------

``` bash
for lib_type in "embryons" "ovaires"
do
    OUT_DIR="scatterplots_for_Droso_2015" BY="1000" NO_LINE="Yes" MIN_COUNTS="10" do_scatterplot.sh "697S13Gn_${lib_type}" "697S13GnS1_${lib_type}" TE TE mt log10 ../TE_lists_Alain/lists_Droso_2015/green.txt,../TE_lists_Alain/lists_Droso_2015/red.txt,../TE_lists_Alain/lists_Droso_2015/black.txt green,red,black 2
done
OUT_DIR="scatterplots_for_Droso_2015" BY="1000" NO_LINE="Yes" MIN_COUNTS="10" do_scatterplot.sh "C2Gn_embryons" "C2GnS1_embryons" TE TE mt log10 ../TE_lists_Alain/lists_Droso_2015/green.txt,../TE_lists_Alain/lists_Droso_2015/red.txt,../TE_lists_Alain/lists_Droso_2015/black.txt green,red,black 2
```

Re-doing scatterplot 24/03/2016
-------------------------------

``` bash
BY="1000" PLOT_DIAGONAL="Yes" MIN_COUNTS="10" do_scatterplot.sh "C2Gn_embryons" "C2GnS1_embryons" TE TE mt log10 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt black,gray,white 2
```

Changing colours and dot size (28/11/2016)
------------------------------------------

``` bash
BY="1000" PLOT_DIAGONAL="Yes" MARKER_SIZE="10" MIN_COUNTS="10" do_scatterplot.sh C2Gn_embryons C2GnS1_embryons TE TE mt log10 ../TE_lists_Alain/TE_Dm_LTR.txt,../TE_lists_Alain/TE_Dm_NLR.txt,../TE_lists_Alain/TE_Dm_DNA.txt black,red,red 2
```
