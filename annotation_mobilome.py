from itertools import ifilterfalse

class Annotation(object):
    __slots__ = (
        "annot_types", "annots", "types", "group", "priority_annots",
        "siRNA_types", "piRNA_groups")
    annot_types = [
        "rRNA", "tRNA", "miRNA", "pre_miRNA",
        "snoRNA", "snRNA", "ncRNA",
        "three_prime_untranslated_region",
        "five_prime_untranslated_region",
        "CDS", "mRNA",
        "transposable_element",
        "not_annotated"]
    #groups = [
    #    "rRNA", "tRNA",
    #    "(pre_)miRNA", "sn(o)RNA", "ncRNA",
    #    "mRNA", "pi-si-TE", "not_annotated"]
    groups = [
        "rRNA", "tRNA",
        "(pre_)miRNA", "sn(o)RNA", "ncRNA",
        "5UTR", "CDS", "mRNA", "3UTR",
        "TE", "not_annotated"]
    # Here are defined types that can be used to determine siRNA reads:
    #siRNA_types = set(["siRNA_cluster"])
    siRNA_types = set()
    # Here are defined groups that can be used
    # to determine potential piRNA reads:
    #piRNA_groups = set([
    #    "pi-si-TE-3UTR", "not_annotated",
    #    "low_score", "unmapped"])
    #piRNA_groups = set(["pi-si-TE-3UTR"])
    piRNA_groups = set()

    @staticmethod
    def set_group(typ):
        if typ == "rRNA":
            return typ
        elif typ == "tRNA":
            return typ
        elif typ == "miRNA":
            return "(pre_)miRNA"
        elif typ == "pre_miRNA":
            return "(pre_)miRNA"
        elif typ == "snoRNA":
            return "sn(o)RNA"
        elif typ == "snRNA":
            return "sn(o)RNA"
        elif typ == "ncRNA":
            return typ
        elif typ == "three_prime_untranslated_region":
            return "3UTR"
        elif typ == "five_prime_untranslated_region":
            return "5UTR"
        elif typ == "CDS":
            return typ
        elif typ == "mRNA":
            return typ
        elif typ == "transposable_element":
            return "TE"
        elif typ == "not_annotated":
            return typ
        else:
            raise NotImplementedError("Annotation type %s is not recognised.\n" % typ)

    @staticmethod
    def lower_priority_tester(group_name):
        index = Annotation.groups.index
        group_index = index(group_name)
        set_group = Annotation.set_group
        def is_lower_priority(annot):
            return index(set_group(annot[0])) > group_index
        return is_lower_priority

    def __init__(self, typ, nam, orientation):
        #self.typ = typ
        self.annots = set([(typ, nam, orientation)])
        self.types = set([typ])
        # /!\
        #TODO: note that no orientation is associated to group or to strict.
        # Orientations are only associated to annotations in annots
        # and in priority_annots
        # weighted orientations? 0.25+/0.75-/00 ?
        self.group = Annotation.set_group(typ)
        #self.strict = None
        self.priority_annots = None
        # May fail if done before all Annotations have been fused
        #self.set_strict()

    def __cmp__(self, other):
        return Annotation.groups.index(other.group) \
            - Annotation.groups.index(self.group)

    def set_strict(self):
        t = self.types
        annots = self.annots
        #index = Annotation.groups.index
        #set_group = Annotation.set_group
        #lpt = Annotation.lower_priority_tester

        if "rRNA" in t:
            is_lower_priority = Annotation.is_lower_than_rRNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "tRNA" in t:
            is_lower_priority = Annotation.is_lower_than_tRNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "miRNA" in t:
            is_lower_priority = Annotation.is_lower_than__pre__miRNA
            # Some miRNA are not annotated as pre-miRNA in flybase data (at least in release 5).
            # e.g.: dme-mir-2a-2
            # Consequently, we have to skip the assertion and hope Flybase people just forgot.
            # assert "pre_miRNA" in t
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "pre_miRNA" in t:
            is_lower_priority = Annotation.is_lower_than__pre__miRNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "snoRNA" in t:
            is_lower_priority = Annotation.is_lower_than_sn_o_RNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "snRNA" in t:
            is_lower_priority = Annotation.is_lower_than_sn_o_RNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "ncRNA" in t:
            is_lower_priority = Annotation.is_lower_than_ncRNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "five_prime_untranslated_region" in t:
            assert "mRNA" not in t, "You should not use mRNA annotations if you want to annotate separately 5' UTR, CDS and 3' UTR\n"
            is_lower_priority = Annotation.is_lower_than_5UTR
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "CDS" in t:
            assert "mRNA" not in t, "You should not use mRNA annotations if you want to annotate separately 5' UTR, CDS and 3' UTR\n"
            is_lower_priority = Annotation.is_lower_than_CDS
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "three_prime_untranslated_region" in t:
            assert "mRNA" not in t, "You should not use mRNA annotations if you want to annotate separately 5' UTR, CDS and 3' UTR\n"
            is_lower_priority = Annotation.is_lower_than_3UTR
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif "mRNA" in t:
            is_lower_priority = Annotation.is_lower_than_mRNA
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        elif t == set(["not_annotated"]):
            is_lower_priority = Annotation.is_lower_than_not_annotated
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        else:
            is_lower_priority = Annotation.is_lower_than_TE
            self.priority_annots = set(ifilterfalse(
                is_lower_priority,
                annots))
        assert self.priority_annots is not None
        
    def fuse(self, other):
        # The returned Annotation will have the highest group in priority
        # of the two (through __cmp__)
        if self >= other:
            self.types.update(other.types)
            self.annots.update(other.annots)
            # wait until all annotations have been gathered
            #self.set_strict()
            return self
        else:
            other.types.update(self.types)
            other.annots.update(self.annots)
            # wait until all annotations have been gathered
            #other.set_strict()
            return other
